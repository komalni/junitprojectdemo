package com.app.demo;

public class Factorial {
	
	public int fact(int a) {
		int factorial=1;
		for(int i = 1; i <= a; ++i)
        {
            factorial *= i;
        }
		return factorial;
	}

}
