package com.app.demo;

import static org.junit.Assert.assertEquals;


import org.junit.*;
public class TestFactorial {
	
	Factorial f = new Factorial();
	@Test
	public void Test() {
	
		assertEquals(1,f.fact(0));
		assertEquals(24,f.fact(4));
		assertEquals(120,f.fact(5));
		assertEquals(720,f.fact(6));
		assertEquals(5040,f.fact(7));
	}

}
