package com.app.demo;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestCalculation {

    Calculation c= new Calculation();
    
	@Test
	public void testingAddition() {
		assertEquals(30, c.addition(27, 3));
	}


	@Test
	public void testingSubtraction() {
		assertEquals(3, c.multiplication(9, 6));
	}

	@Test
	public void testingMultiplication() {
		assertEquals(12, c.multiplication(4, 3));
	}

	@Test
	public void testingDivision() {
		assertEquals(6, c.division(30, 5));
	}
	
}
